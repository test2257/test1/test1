FROM harbor.csljc.com/${dockerfile_tag}:${dockerfile_ver}

MAINTAINER CSLJC ${dockerfile_tag}-${dockerfile_ver}

LABEL name="CentOS Service Image ${SVC_LABEL} From ${dockerfile_tag}:${dockerfile_ver}" \

    vendor="CSLJC" \

    license="GPLv9" \

    build-date="${BUILD_DATE}"

VOLUME ["/data"]

WORKDIR /app

ADD ${SVC_TAG}.tar.gz /app

ENV APP_DOCKER_VERSION ${SVC_LABEL}

ENV SV_HOME /app/${SVC_TAG}
